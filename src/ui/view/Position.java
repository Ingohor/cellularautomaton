package ui.view;

import javax.swing.*;

/**
 * Created by Aleksander Granowski on 17.05.2016.
 */

public class Position
{
    private JPanel parent;
    public Position(JPanel view)
    {
        parent = view;
        calculateOffset();
    }
    public static final int MINSCALE = 2;
    public static final int MAXSCALE = 60;

    private int x = 0;
    public int getX()
    {
        return x;
    }

    public int getX(int offset)
    {
        return offset * scale - x;
    }
    public void setX(int value)
    {
        x = value;
        calculateOffset();
    }

    private int y = 0;
    public int getY()
    {
        return y;
    }

    public int getY(int offset)
    {
        return offset * scale - y;
    }
    public void setY(int value)
    {
        y = value;
        calculateOffset();
    }

    private int scale = 32;
    public int getScale()
    {
        return scale;
    }
    public void setScale(int value)
    {
        scale = value;
        calculateOffset();
    }

    public void calculateOffset()
    {
        min_x = (x) / scale;
        max_x = (int) Math.ceil(((double) (parent.getWidth() + x) / scale));
        min_y = (y) / scale;
        max_y = (int) Math.ceil(((double) (parent.getHeight() + y) / scale));
    }

    private int min_x;
    public int getMinX()
    {
        return min_x;
    }
    private int max_x;
    public int getMaxX()
    {
        if(max_x == 0) calculateOffset();
        return max_x;
    }
    private int min_y;
    public int getMinY()
    {
        return min_y;
    }
    private int max_y;
    public int getMaxY()
    {
        if(max_y == 0) calculateOffset();
        return max_y;
    }

}
