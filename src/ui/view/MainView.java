package ui.view;

import core.interfaces.ICellAutomation;
import core.interfaces.IModule;
import core.model.Element;
import wireworld.WireModule;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;

/**
 * Created by Aleksander Granowski on 18.05.2016.
 */
public class MainView extends JFrame {
    private JButton startButton;
    private JPanel panel1;
    private JList list1;
    private GridView grid;
    private ICellAutomation cellAutomation;
    private IModule currentModule;
    private Timer updatetimer;
    public MainView() {
        setContentPane(panel1);
        setTitle("Automat komórkowy(Wireworld)");
        list1.setListData(currentModule.getBaseElements());
        list1.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if(list1.getSelectedValue() != null) {
                    grid.setSelectElement(new ElementView((Element) list1.getSelectedValue(), grid.getPosition()));
                    grid.getSelectElement().IsSelect = true;
                    grid.getSelectElement().IsCreate = true;
                }
                grid.repaint();
            }
        });

        list1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
                {
                    grid.setSelectElement(null);
                    list1.clearSelection();
                }
            }
        });

        updatetimer  = new Timer(10, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cellAutomation.next();
                grid.update(cellAutomation.getGrid());
            }
        });
        startButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if(updatetimer.isRunning())
                {
                    updatetimer.stop();
                    startButton.setText("Start");
                }else{
                    updatetimer.start();
                    startButton.setText("Stop");
                }
            }
        });
        createMenuBar();
    }

    private void createMenuBar() {

        JMenuBar menubar = new JMenuBar();
        JMenu file = new JMenu("File");

        final JMenuItem eMenuItem1 = new JMenuItem("Create");
        eMenuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cellAutomation.clear();
                grid.update(cellAutomation.getGrid());
                repaint();
            }
        });

        JMenuItem eMenuItem2 = new JMenuItem("Open");
        eMenuItem2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser openFile = new JFileChooser();
                openFile.addChoosableFileFilter(new FileNameExtensionFilter("Text file", "txt"));
                openFile.addChoosableFileFilter(new FileNameExtensionFilter("Image", "png"));
                openFile.setFileFilter(openFile.getChoosableFileFilters()[1]);
                openFile.setMultiSelectionEnabled(false);
                openFile.setAcceptAllFileFilterUsed(false);
                openFile.showOpenDialog(null);
                if(openFile.getSelectedFile() == null) return;

                if(openFile.getFileFilter().getDescription() == "Text file")
                {
                    cellAutomation.load(currentModule.getIOManagers()[0], openFile.getSelectedFile().getAbsolutePath());
                }

                if(openFile.getFileFilter().getDescription() == "Image")
                {
                    cellAutomation.load(currentModule.getIOManagers()[1], openFile.getSelectedFile().getAbsolutePath());

                }
                grid.update(cellAutomation.getGrid());
                repaint();
            }
        });
        JMenuItem eMenuItem3 = new JMenuItem("Save");

        eMenuItem3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser openFile = new JFileChooser();
                openFile.addChoosableFileFilter(new FileNameExtensionFilter("Text file", "txt"));
                openFile.addChoosableFileFilter(new FileNameExtensionFilter("Image", "png"));
                openFile.setFileFilter(openFile.getChoosableFileFilters()[1]);
                openFile.setMultiSelectionEnabled(false);
                openFile.setAcceptAllFileFilterUsed(false);
                openFile.showSaveDialog(null);
                if(openFile.getSelectedFile() == null) return;

                if(openFile.getFileFilter().getDescription() == "Text file")
                {
                    cellAutomation.save(currentModule.getIOManagers()[0], openFile.getSelectedFile().getAbsolutePath());
                }

                if(openFile.getFileFilter().getDescription() == "Image")
                {
                    cellAutomation.save(currentModule.getIOManagers()[1], openFile.getSelectedFile().getAbsolutePath());
                }

                grid.update(cellAutomation.getGrid());
                repaint();
            }
        });

        file.add(eMenuItem1);
        file.add(eMenuItem2);
        file.add(eMenuItem3);
        menubar.add(file);
        setJMenuBar(menubar);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        currentModule = new WireModule();
        cellAutomation = currentModule.createCellAutomation();
        grid = new GridView(cellAutomation.getGrid());
    }
}
