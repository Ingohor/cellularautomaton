package ui.view;

import core.interfaces.IGrid;
import core.model.Element;
import ui.controller.MouseController;
import ui.controller.MouseDragController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.LinkedList;

public class GridView extends JPanel {

    private LinkedList<ElementView> elementViews = new LinkedList<>();
    private ElementView selectElement;
    private Position position = new Position(this);
    private IGrid grid;
    public GridView(IGrid grid) {

        this.grid = grid;
        for (Element element : grid.getElements())
        {
            getElementViews().add(new ElementView(element, getPosition()));
        }


        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent componentEvent) {
                getPosition().calculateOffset();
                super.componentResized(componentEvent);
            }
        });
        MouseController mouseController = new MouseController(this);
        MouseDragController dragController = new MouseDragController(this);
        addMouseListener(mouseController);

        addMouseListener(dragController);
        addMouseMotionListener(dragController);

        addMouseWheelListener(mouseController);
        addMouseMotionListener(mouseController);
    }


    public Position getPosition()
    {
        return position;
    }

    protected void paintComponent(Graphics g) {
        paintGrid(g);
        painElements(g);
    }
    private void paintGrid(Graphics g)
    {
        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.gray);

        if(position.getScale() < 5) return;
        for (int xx = -getPosition().getX(); xx <= getWidth() + getPosition().getX(); xx += getPosition().getScale()) {
            g.drawLine(xx, 0, xx, getHeight());
        }
        for (int yy = -getPosition().getY(); yy <= getHeight() + getPosition().getY(); yy += getPosition().getScale()) {
            g.drawLine(0, yy, getWidth(), yy);
        }
    }

    private void painElements(Graphics g) {
        for (ElementView element : getElementViews()) {
            if (getPosition().getMinX() <= element.element.Width + element.element.X && getPosition().getMaxX() >= element.element.X) {
                if (getPosition().getMinY() <= element.element.Y + element.element.Y && getPosition().getMaxY() >= element.element.Y) {
                    element.paint(g);
                }
            }
        }
        if(getSelectElement() != null && getSelectElement().IsCreate)
        {
            getSelectElement().paint(g);
        }
    }

    public LinkedList<ElementView> getElementViews() {
        return elementViews;
    }

    public void setElementViews(LinkedList<ElementView> elementViews) {
        this.elementViews = elementViews;
    }

    public ElementView getSelectElement() {
        return selectElement;
    }

    public void setSelectElement(ElementView selectElement) {
        this.selectElement = selectElement;
    }

    public IGrid getGrid()
    {
        return grid;
    }

    public void update(IGrid grid)
    {
        this.grid = grid;
        update();
    }
    public void update()
    {
        getElementViews().clear();
        for (Element element : grid.getElements()) {
            getElementViews().add(new ElementView(element, getPosition()));
        }
        repaint();
    }
}

