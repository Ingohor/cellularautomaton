package ui.view;

import core.model.Element;

import java.awt.*;

/**
 * Created by Aleksander Granowski on 17.05.2016.
 */
public class ElementView {

    public Element element;
    public Position position;
    public boolean IsSelect = false;
    public boolean IsCreate = false;
    public ElementView(Element element, Position pos)
    {
        this.element = element;
        position = pos;
    }

    public void paint(Graphics g) {
        if(IsSelect)
        {
            g.setColor(Color.red);
            int x = position.getX(element.X);
            int y = position.getY(element.Y);
            g.drawRect(x, y, element.Width * position.getScale(), element.Height * position.getScale());
        }
        for (int i = 0; i < element.Width && i <= position.getMaxX(); i++) {
            for (int j = 0; j < element.Height && j <= position.getMaxY(); j++) {
                if (element.Cells[j][i] !=  null) {
                    g.setColor(element.Cells[j][i].getColor());
                    int x = position.getX(element.X + i) + 1;
                    int y = position.getY(element.Y + j) + 1;
                    g.fillRect(x, y, position.getScale() - 1, position.getScale() - 1);
                }
            }
        }
    }

    public boolean isArea(int x, int y)
    {
        if(x >= element.X && x < element.X + element.Width)
        {
            if(y >= element.Y && y < element.Y + element.Height)
            {
                return true;
            }
        }
        return false;
    }

    public boolean isArea(ElementView e)
    {
        for (int x = e.element.X; x < e.element.X + e.element.Width; x++)
        {
            for (int y = e.element.Y; y < e.element.Y + e.element.Height; y++) {
                if(isArea(x, y)) return true;
            }
        }
        return false;
    }
}
