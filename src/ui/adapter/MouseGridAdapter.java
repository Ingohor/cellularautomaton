package ui.adapter;

import ui.event.MouseGridEvent;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Aleksander Granowski on 23.05.2016.
 */
public abstract class MouseGridAdapter{

    private MouseAdapter mouseAdapter;
    public MouseGridAdapter()
    {
        mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                super.mouseMoved(mouseEvent);
            }

            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                super.mouseDragged(mouseEvent);
            }

            @Override
            public void mouseClicked(MouseEvent mouseEvent)
            {

            }
        };
    }
    public abstract void mouseMoved(MouseGridEvent mouseEvent);
    public abstract void mouseDragged(MouseGridEvent mouseEvent);
    public abstract void mouseClicked(MouseGridEvent mouseEvent);
}
