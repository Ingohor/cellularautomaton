package ui.controller;

import ui.view.GridView;
import ui.view.Position;

import java.awt.event.*;

/**
 * Created by Aleksander Granowski on 17.05.2016.
 */
public class MouseController extends MouseAdapter implements MouseWheelListener, MouseMotionListener {

    private int diffX = 0;
    private int diffY = 0;
    private GridView view;
    private Position position;

    public MouseController(GridView grid)
    {
        view = grid;
        position = grid.getPosition();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (mouseEvent.isControlDown()) {
            diffX = mouseEvent.getX();
            diffY = mouseEvent.getY();
        }
    }


    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        if (mouseWheelEvent.isControlDown()) {
            int scale = position.getScale() - (mouseWheelEvent.getUnitsToScroll() < 0 ? -1 : 1);
            if (scale < Position.MINSCALE) scale = Position.MINSCALE;
            if (scale > Position.MAXSCALE) scale = Position.MAXSCALE;
            position.setScale(scale);
            view.repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (mouseEvent.isControlDown()) {
            int x  = position.getX() + (mouseEvent.getX() - diffX) * (-1);
            int y  = position.getY() + (mouseEvent.getY() - diffY) * (-1);
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            position.setX(x);
            position.setY(y);
            diffX = mouseEvent.getX();
            diffY = mouseEvent.getY();
            view.repaint();
        }
    }
}
