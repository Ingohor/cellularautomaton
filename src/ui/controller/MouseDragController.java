package ui.controller;

import ui.view.ElementView;
import ui.view.GridView;
import ui.view.Position;
import wireworld.elements.CellElement;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

/**
 * Created by Aleksander Granowski on 17.05.2016.
 */
public class MouseDragController extends MouseAdapter {

    private Position position;
    private LinkedList<ElementView> elements;
    private GridView gridView;
    public MouseDragController(GridView gridView)
    {
        this.gridView = gridView;
        this.elements = gridView.getElementViews();
        this.position = gridView.getPosition();

    }
    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {
        ElementView selectElement = gridView.getSelectElement();
        if(mouseEvent.getButton() == MouseEvent.BUTTON1) { // Left Button
            if (selectElement != null) {
                for (ElementView element : elements) {
                    if (element == selectElement) continue;

                    if (selectElement.isArea(element))
                    {
                        if(selectElement.element instanceof CellElement)
                        {
                            try {
                                int x = (position.getMinX() / position.getScale() + (mouseEvent.getX() / position.getScale())) - (position.getX(element.element.X) / position.getScale());
                                int y = (position.getMinY() / position.getScale() + (mouseEvent.getY() / position.getScale())) - (position.getX(element.element.Y) / position.getScale());
                                element.element.Cells[y][x] = selectElement.element.Cells[0][0].getClass().newInstance();
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                        return;
                    }
                }
                if (selectElement.IsCreate) {
                    gridView.getGrid().getElements().add(selectElement.element.create());
                    gridView.update();
                    return;
                }
                selectElement.IsSelect = false;
                gridView.setSelectElement(null);
                gridView.repaint();
                return;
            }
            if (mouseEvent.getClickCount() >= 2) {
                int x = position.getMinX() + (mouseEvent.getX() / position.getScale());
                int y = position.getMinY() + (mouseEvent.getY() / position.getScale());
                for (ElementView element : elements) {
                    if (element.isArea(x, y)) {
                        element.IsSelect = true;
                        gridView.setSelectElement(element);
                        gridView.repaint();
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        ElementView selectElement = gridView.getSelectElement();
        if(selectElement != null)
        {
            selectElement.element.X = position.getMinX() + (mouseEvent.getX() / position.getScale());
            selectElement.element.Y = position.getMinY() + (mouseEvent.getY() / position.getScale());
            gridView.repaint();
        }
    }

}
