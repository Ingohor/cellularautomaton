package core.model;

import java.awt.*;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public abstract class Cell {

    public abstract Color getColor();
}
