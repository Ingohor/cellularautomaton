package core.model;

/**
 * Created by Aleksander Granowski on 18.05.2016.
 */
public abstract class Element {
    public String Name;
    public int X;
    public int Y;
    public int Width;
    public int Height;
    public Cell[][] Cells;

    public Element()
    {

    }

    public Element create()
    {
        try {
            Element element =  this.getClass().newInstance();
            element.X = this.X;
            element.Y = this.Y;
            return element;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString()
    {
        return Name;
    }

    public String serialization()
    {
        return String.format("%s: %d, %d", Name, X, Y);
    }
}
