package core.interfaces;

import core.model.Element;

import java.util.List;


/**
 * Created by Aleksander Granowski on 26.05.2016.
 */
public interface IOManager {
    List<Element> load(String path);
    void save(List<Element> elements, String path);
}
