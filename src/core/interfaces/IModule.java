package core.interfaces;

import core.model.Element;

/**
 * Created by Aleksander Granowski on 26.05.2016.
 */
public interface IModule {
    ICellAutomation createCellAutomation();
    IOManager[] getIOManagers();
    Element[] getBaseElements();
}
