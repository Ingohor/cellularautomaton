package core.interfaces;

/**
 * Created by Aleksander Granowski on 22.05.2016.
 */
public interface ICellAutomation {
    IGrid getGrid();
    void next();
    void clear();
    void load(IOManager manager, String arg);
    void save(IOManager manager, String arg);
}
