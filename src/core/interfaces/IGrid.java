package core.interfaces;

import core.model.Element;

import java.util.List;

/**
 * Created by Aleksander Granowski on 31.05.2016.
 */
public interface IGrid {
   // List<Cell> getCells();
    List<Element> getElements();
}
