package sample;

import ui.view.MainView;

import javax.swing.*;

public class Main {



    public static void main(String[] args) {

        JFrame f = new MainView();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //  f.setContentPane(new MainView());
        f.setResizable(true);
        f.setSize(1000, 1000);
        f.setLocationRelativeTo(null);
//      //  f.add(new GridView());
        f.setVisible(true);
    }
}
