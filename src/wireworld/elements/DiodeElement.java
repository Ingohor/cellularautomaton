package wireworld.elements;

import core.model.Cell;
import core.model.Element;
import wireworld.cells.ConductorCell;

/**
 * Created by Aleksander Granowski on 13.06.2016.
 */
public class DiodeElement extends Element {
    public DiodeElement()
    {
        Name = "Diode";
        X = 0;
        Y = 0;
        Width  = 6;
        Height = 3;
        Cells = new Cell[Height][Width];
        Cells[0] = new Cell[] { null, null , new ConductorCell(), new ConductorCell(), null, null};
        Cells[1] = new Cell[] { new ConductorCell(), new ConductorCell(), new ConductorCell(), null, new ConductorCell(), new ConductorCell()};
        Cells[2] = new Cell[] { null, null, new ConductorCell(), new ConductorCell(), null, null};
    }
}
