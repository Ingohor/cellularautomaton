package wireworld.elements;

import core.model.Element;
import wireworld.cells.ConductorCell;
import wireworld.cells.ElectronHeadCell;
import wireworld.cells.ElectronTailCell;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public class ElementFactory {

    static Map<String, Class> m;
    static {
        m = new HashMap();
        m.put("ELECTRONHEADCELL", CellElement.class);
        m.put("ELECTRONTAILCELL", CellElement.class);
        m.put("CONDUCTORCELL", CellElement.class);
        m.put("OR", ORGateElement.class);
        m.put("DIODE", DiodeElement.class);
    }
    public static Element createElement(String name, int x, int y)
    {
        try {
            if(m.containsKey(name.toUpperCase()))
            {
                Element element = null;
                if(m.get(name.toUpperCase()) == CellElement.class)
                {
                    if( name.toUpperCase().equals("ELECTRONHEADCELL")) element = new CellElement(new ElectronHeadCell());
                    if( name.toUpperCase().equals("ELECTRONTAILCELL")) element = new CellElement(new ElectronTailCell());
                    if( name.toUpperCase().equals("CONDUCTORCELL")) element = new CellElement(new ConductorCell());
                    element.X = x;
                    element.Y = y;
                }
                else
                {
                    element = (Element) m.get(name.toUpperCase()).newInstance();
                    element.X = x;
                    element.Y = y;
                }
                return element;
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
