package wireworld.elements;

import core.model.Cell;
import core.model.Element;
import wireworld.cells.ConductorCell;

/**
 * Created by Aleksander Granowski on 26.05.2016.
 */
public class ORGateElement extends Element {

    public ORGateElement()
    {
        Name = "OR";
        X = 0;
        Y = 0;
        Width = Height = 5;
        Cells = new Cell[5][5];
        Cells[0] = new Cell[] { new ConductorCell(), new ConductorCell(), null, null, null};
        Cells[1] = new Cell[] { null, null, new ConductorCell(), null, null};
        Cells[2] = new Cell[] { null, new ConductorCell(), new ConductorCell(), new ConductorCell(), new ConductorCell()};
        Cells[3] = new Cell[] { null, null, new ConductorCell(), null, null};
        Cells[4] = new Cell[] { new ConductorCell(), new ConductorCell(), null, null, null};
    }
}
