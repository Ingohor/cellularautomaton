package wireworld.elements;

import core.model.Cell;
import core.model.Element;
import wireworld.cells.ConductorCell;
import wireworld.cells.ElectronHeadCell;
import wireworld.cells.ElectronTailCell;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public class CellElement extends Element{


    public CellElement(Cell cell)
    {
        if(cell instanceof ConductorCell)     Name = "Conductor";
        if(cell instanceof ElectronTailCell)  Name = "ElectronTail";
        if(cell instanceof ElectronHeadCell)  Name = "ElectronHead";

        X = 0;
        Y = 0;
        Width = 1;
        Height = 1;
        Cells = new Cell[1][1];
        Cells[0][0] = cell;

    }

    @Override
    public Element create()
    {
        try {
            Element element = new CellElement(Cells[0][0].getClass().newInstance());
            element.X = this.X;
            element.Y = this.Y;
            return element;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String serialization()
    {
        String value = "Cell";
        if(Cells[0][0] instanceof ElectronHeadCell) value = "ElectronHeadCell";
        if(Cells[0][0] instanceof ElectronTailCell) value = "ElectronTailCell";
        if(Cells[0][0] instanceof ConductorCell) value = "ConductorCell";
        return String.format("%s: %d, %d", value, X, Y);
    }
}
