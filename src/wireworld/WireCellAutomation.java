package wireworld;

import core.interfaces.ICellAutomation;
import core.interfaces.IGrid;
import core.interfaces.IOManager;
import core.model.Cell;
import core.model.Element;
import wireworld.cells.ConductorCell;
import wireworld.cells.ElectronHeadCell;
import wireworld.cells.ElectronTailCell;
import wireworld.elements.CellElement;

import java.util.*;

/**
 * Created by Aleksander Granowski on 26.05.2016.
 */
class WireCellAutomation implements ICellAutomation {

    int head = 3;
    private IGrid grid;

    public WireCellAutomation() {
        grid = new Grid();
    }

    @Override
    public IGrid getGrid() {
        return grid;
    }

    public Map<String, CellElement> db;

    private void converttoCell() {
        LinkedList<Element> elements = new LinkedList<>();
        db = new LinkedHashMap<String, CellElement>();
        for (Element e : getGrid().getElements()) {
            if (e instanceof CellElement) {
                elements.add(e);
                db.put(e.X + ":" + e.Y, (CellElement) e);
                continue;
            }

            for (int x = 0; x < e.Width; x++) {
                for (int y = 0; y < e.Height; y++) {
                    if (e.Cells[y][x] == null) continue;
                    CellElement cellElement = new CellElement(e.Cells[y][x]);
                    cellElement.X = e.X + x;
                    cellElement.Y = e.Y + y;
                    elements.add(cellElement);
                    db.put(cellElement.X + ":" + cellElement.Y, cellElement);
                }
            }
        }
        getGrid().getElements().clear();
        getGrid().getElements().addAll(elements);
    }

    int it = 0;
    @Override
    public void next() {
        converttoCell();
        for (int a = 0; a < 1; a++) {
            Map<Element, Cell> h = new HashMap();
            for (Element e : getGrid().getElements()) {
                if (e.Cells[0][0] instanceof ConductorCell) {
                    int c = getNearby2(e);
                    if (c == 1 || c == 2) {
                        h.put(e, new ElectronHeadCell());
                    }
                }

                if (e.Cells[0][0] instanceof ElectronHeadCell) {
                    h.put(e, new ElectronTailCell());
                }

                if (e.Cells[0][0] instanceof ElectronTailCell) {
                    h.put(e, new ConductorCell());
                }
            }
            for (Element element : h.keySet()) {
                element.Cells[0][0] = h.get(element);
            }
        }
        it++;
        System.out.println(it);
    }

    public void work(Element e, Map<Element, Cell> h) {
        if (e.Cells[0][0] instanceof ConductorCell) {
            List<CellElement> nearby = getNearby(e);
            int c = countClass(nearby, ElectronHeadCell.class);
            if (c == 1 || c == 2) {
                h.put(e, new ElectronHeadCell());
            }
        }

        if (e.Cells[0][0] instanceof ElectronHeadCell) {
            h.put(e, new ElectronTailCell());
        }

        if (e.Cells[0][0] instanceof ElectronTailCell) {
            h.put(e, new ConductorCell());
        }

    }


    private int countClass(List<CellElement> list, Class t)
    {
        int result = 0;
        for (CellElement c : list)
        {
            if(t.isInstance(c.Cells[0][0]))
            {
                result ++;
            }
        }
        return result ;
    }

    private int getNearby2(Element element)
    {
        int result = 0;
        for (int offsetx = -1; offsetx <= 1; offsetx++)
        {
            for (int offsety = -1; offsety <= 1; offsety++)
            {
                CellElement cellElement = db.get((element.X + offsetx) + ":" + (element.Y + offsety));
                if(cellElement == null) continue;
                if(cellElement.Cells[0][0] instanceof ElectronHeadCell)
                {
                    result ++;
                }
            }
        }
        return result;
    }

    private List<CellElement> getNearby(Element element)
    {
        List<CellElement> list = new LinkedList<>();
        for (int offsetx = -1; offsetx <= 1; offsetx++)
        {
            for (int offsety = -1; offsety <= 1; offsety++)
            {
            //    if(offsetx == 0 && offsety == 0) continue;
                CellElement cellElement = db.get((element.X + offsetx) + ":" + (element.Y + offsety));
                if(cellElement == null) continue;
          //      if(cellElement == element)
           //         continue;
                list.add(cellElement);
            }
        }
        return list;
    }
    @Override
    public void clear()
    {
        grid.getElements().clear();
    }
    @Override
    public void load(IOManager manager, String arg) {
        grid.getElements().clear();
        grid.getElements().addAll(manager.load(arg));
    }

    @Override
    public void save(IOManager manager, String arg) {
        converttoCell();
        manager.save(grid.getElements(), arg);
    }

}