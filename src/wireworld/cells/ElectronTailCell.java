package wireworld.cells;

import core.model.Cell;

import java.awt.*;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public class ElectronTailCell extends Cell {
    @Override
    public Color getColor() {
        return Color.red;
    }
}
