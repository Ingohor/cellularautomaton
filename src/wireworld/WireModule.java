package wireworld;

import core.interfaces.ICellAutomation;
import core.interfaces.IModule;
import core.interfaces.IOManager;
import core.model.Element;
import wireworld.cells.ConductorCell;
import wireworld.cells.ElectronHeadCell;
import wireworld.cells.ElectronTailCell;
import wireworld.elements.CellElement;
import wireworld.elements.DiodeElement;
import wireworld.elements.ORGateElement;
import wireworld.io.FileIOManager;
import wireworld.io.ImageIOManager;

/**
 * Created by Aleksander Granowski on 26.05.2016.
 */
public class WireModule implements IModule {
    @Override
    public ICellAutomation createCellAutomation() {
        return new WireCellAutomation();
    }

    @Override
    public IOManager[] getIOManagers() {
        return new IOManager[]
                {
                        new FileIOManager(),
                        new ImageIOManager(),
                };
    }

    @Override
    public Element[] getBaseElements() {
        return new Element[]
                {
                        new CellElement(new ConductorCell()),
                        new CellElement(new ElectronHeadCell()),
                        new CellElement(new ElectronTailCell()),
                        new ORGateElement(),
                        new DiodeElement()
                };
    }
}
