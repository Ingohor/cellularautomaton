package wireworld;

import core.interfaces.IGrid;
import core.model.Element;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aleksander Granowski on 31.05.2016.
 */
public class Grid implements IGrid {

    private List<Element> elements = new LinkedList<>();
    public Grid()
    {

    }

    public List<Element> getElements() {
        return elements;
    }

}
