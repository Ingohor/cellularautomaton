package wireworld.io;

import core.interfaces.IOManager;
import core.model.Element;
import wireworld.elements.ElementFactory;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public class FileIOManager implements IOManager {
    @Override
    public List<Element> load(String path) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            if(reader.ready())
            {
                List<Element> elements = new LinkedList();
                String data;
                while ((data = reader.readLine()) != null)
                {
                    Pattern p = Pattern.compile("([A-z]*): (\\d*), (\\d*)");
                    Matcher m = p.matcher(data);
                    if(m.find()) {
                        elements.add(ElementFactory.createElement(m.group(1), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3))));
                    }
                }
                reader.close();
                return elements;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void save(List<Element> elements, String path) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(path));
            for (Element e: elements)
            {
                writer.write(e.serialization());
                writer.newLine();
                writer.flush();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
