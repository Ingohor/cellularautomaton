package wireworld.io;

import core.interfaces.IOManager;
import core.model.Element;
import wireworld.cells.ConductorCell;
import wireworld.cells.ElectronHeadCell;
import wireworld.cells.ElectronTailCell;
import wireworld.elements.CellElement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Aleksander Granowski on 07.06.2016.
 */
public class ImageIOManager implements IOManager {

    private final int ELECTRON_TAIL_VALUE = -7763575;
    private final int ELECTRON_HEAD_VALUE = -1;
    private final int CONDUCTOR_VALUE = -10263709;
    @Override
    public List<Element> load(String path) {
        try {
            BufferedImage image = ImageIO.read(new File(path));
            List<Element> elements = new LinkedList();
            for (int x = 0; x < image.getWidth(); x++)
            {
                for (int y = 0; y < image.getHeight(); y++) {
                    int clr =  image.getRGB(x,y);
                    Element e = null;
                    if(clr == ELECTRON_TAIL_VALUE)
                    {
                        e = new CellElement(new ElectronTailCell());
                        e.X = x;
                        e.Y = y;
                    }
                    if(clr == ELECTRON_HEAD_VALUE)
                    {
                        e = new CellElement(new ElectronHeadCell());
                        e.X = x;
                        e.Y = y;
                    }
                    if(clr == CONDUCTOR_VALUE)
                    {
                        e = new CellElement(new ConductorCell());
                        e.X = x;
                        e.Y = y;
                    }
                    if(e == null)
                    {
                        continue;
                    }
                    elements.add(e);
                }
            }
            return elements;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void save(List<Element> elements, String path) {
        try {
            int maxx = 0, maxy = 0;
            for (Element e: elements)
            {
                if(e.X > maxx) maxx = e.X;
                if(e.Y > maxy) maxy = e.Y;
            }
            BufferedImage image = new BufferedImage(maxx +1, maxy +1, BufferedImage.TYPE_INT_ARGB);
            for (Element e: elements)
            {
                int v = 0;
                if(e.Cells[0][0] instanceof ConductorCell) v = CONDUCTOR_VALUE;
                if(e.Cells[0][0] instanceof ElectronTailCell) v = ELECTRON_TAIL_VALUE;
                if(e.Cells[0][0] instanceof ElectronHeadCell) v = ELECTRON_HEAD_VALUE;
                image.setRGB(e.X, e.Y, v);
            }
            ImageIO.write(image, "png", new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
